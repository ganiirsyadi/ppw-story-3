const menuBtn = document.querySelector('.navbar-toggler');
const nav = document.querySelector('nav');
const strip = document.querySelectorAll('.nav-item-strip');
const navItem = document.querySelectorAll('.nav-item');
const header = document.querySelector('.title');
const navBrand = document.querySelector('.navbar-brand')
const logo = document.querySelector('.logo')
const logoDark = document.querySelector('.logo-dark')


menuBtn.addEventListener("click", function () {  
    strip.forEach(element => {
        element.style.display = "none";
    });
    navItem.forEach(element => {
        element.style.marginTop = "1.5em"
    })
    
    navItem[navItem.length -1].style.marginBottom = "1em"    
})

const headerOptions = {
};
const headerObserver = new IntersectionObserver
(function (
    entries, 
    headerObserver
    ) {  
        entries.forEach(entry => {
            if (!entry.isIntersecting) {
                nav.classList.add("bg-light");
                nav.classList.add("navbar-light");
                nav.classList.remove("navbar-dark");
                logoDark.style.display ="inline-block"
                logo.style.display ="none"
            } else {
                nav.classList.remove("bg-light");
                nav.classList.remove("navbar-light");
                nav.classList.add("navbar-dark");
                logo.style.display ="inline-block"
                logoDark.style.display ="none"
            }
        });
    },
    headerOptions)

headerObserver.observe(header)
